<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGoalsFieldsInSeasonTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('season_team', function (Blueprint $table) {
            $table->unsignedInteger('goals_count')->default(0)->after('scope');
            $table->unsignedInteger('conceded_goals_count')->default(0)->after('goals_count');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('season_team', function (Blueprint $table) {
            $table->dropColumn('conceded_goals_count');
            $table->dropColumn('goals_count');
        });
    }
}
