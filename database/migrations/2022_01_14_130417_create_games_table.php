<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('season_id');
            $table->unsignedBigInteger('team_a_id');
            $table->unsignedBigInteger('team_b_id');
            $table->unsignedTinyInteger('scope_a')->default(0);
            $table->unsignedTinyInteger('scope_b')->default(0);
            $table->unsignedBigInteger('wined_team_id')->nullable();
            $table->unsignedBigInteger('stadium_id');
            $table->timestamp('match_start_at');
            $table->timestamps();

            $table->foreign('team_a_id')
                ->references('id')
                ->on('teams')
                ->onDelete('cascade');

            $table->foreign('team_b_id')
                ->references('id')
                ->on('teams')
                ->onDelete('cascade');

            $table->foreign('wined_team_id')
                ->references('id')
                ->on('teams')
                ->onDelete('cascade');

            $table->foreign('season_id')
                ->references('id')
                ->on('seasons')
                ->onDelete('cascade');

            $table->foreign('stadium_id')
                ->references('id')
                ->on('stadiums')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
