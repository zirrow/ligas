<?php

namespace Database\Seeders;

use App\Models\Team;
use Illuminate\Database\Seeder;

class TeamsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Team::insert([
            'name' => 'Manchester United',
            'stadium_id' => 1
        ]);

        Team::insert([
            'name' => 'Tottenham Hotspur',
            'stadium_id' => 2
        ]);

        Team::insert([
            'name' => 'Arsenal',
            'stadium_id' => 3
        ]);

        Team::insert([
            'name' => 'West Ham United',
            'stadium_id' => 4
        ]);

        Team::insert([
            'name' => 'Manchester City',
            'stadium_id' => 5
        ]);

        Team::insert([
            'name' => 'Liverpool City',
            'stadium_id' => 6
        ]);

        Team::insert([
            'name' => 'Newcastle United',
            'stadium_id' => 7
        ]);

        Team::insert([
            'name' => 'Chelsea',
            'stadium_id' => 10
        ]);

        Team::insert([
            'name' => 'Everton',
            'stadium_id' => 12
        ]);
    }
}
