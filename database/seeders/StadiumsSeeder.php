<?php

namespace Database\Seeders;

use App\Models\Stadium;
use Illuminate\Database\Seeder;

class StadiumsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Stadium::insert([
            'id' => 1,
            'name' => 'Old Trafford'
        ]);

        Stadium::insert([
            'id' => 2,
            'name' => 'Tottenham Hotspur Stadium'
        ]);

        Stadium::insert([
            'id' => 3,
            'name' => 'Emirates Stadium'
        ]);

        Stadium::insert([
            'id' => 4,
            'name' => 'London Stadium'
        ]);

        Stadium::insert([
            'id' => 5,
            'name' => 'Etihad Stadium'
        ]);

        Stadium::insert([
            'id' => 6,
            'name' => 'Anfield'
        ]);

        Stadium::insert([
            'id' => 7,
            'name' => 'St James` Park'
        ]);

        Stadium::insert([
            'id' => 8,
            'name' => 'Stadium of Light'
        ]);

        Stadium::insert([
            'id' => 9,
            'name' => 'Villa Park'
        ]);

        Stadium::insert([
            'id' => 10,
            'name' => 'Stamford Bridge'
        ]);

        Stadium::insert([
            'id' => 12,
            'name' => 'Goodison Park'
        ]);

        Stadium::insert([
            'id' => 13,
            'name' => 'Hillsborough Stadium'
        ]);

        Stadium::insert([
            'id' => 14,
            'name' => 'Elland Road'
        ]);

        Stadium::insert([
            'id' => 15,
            'name' => 'Riverside Stadium'
        ]);

        Stadium::insert([
            'id' => 16,
            'name' => 'Pride Park Stadium'
        ]);

        Stadium::insert([
            'id' => 17,
            'name' => 'Bramall Lane Stadium'
        ]);

        Stadium::insert([
            'id' => 18,
            'name' => 'St Mary`s Stadium'
        ]);

        Stadium::insert([
            'id' => 19,
            'name' => 'Coventry Building Society Arena'
        ]);

        Stadium::insert([
            'id' => 20,
            'name' => 'King Power Stadium'
        ]);

        Stadium::insert([
            'id' => 21,
            'name' => 'Ewood Park'
        ]);

        Stadium::insert([
            'id' => 22,
            'name' => 'City Ground'
        ]);

        Stadium::insert([
            'id' => 23,
            'name' => 'Portman Road'
        ]);

        Stadium::insert([
            'id' => 24,
            'name' => 'St Andrews'
        ]);

        Stadium::insert([
            'id' => 25,
            'name' => 'University of Bolton Stadium'
        ]);

        Stadium::insert([
            'id' => 26,
            'name' => 'Molineux Stadium'
        ]);

        Stadium::insert([
            'id' => 27,
            'name' => 'Bet365 Stadium'
        ]);

        Stadium::insert([
            'id' => 28,
            'name' => 'The Hawthorns'
        ]);

        Stadium::insert([
            'id' => 29,
            'name' => 'The Valley'
        ]);

        Stadium::insert([
            'id' => 30,
            'name' => 'Cardiff City Stadium'
        ]);

        Stadium::insert([
            'id' => 31,
            'name' => 'Craven Cottage'
        ]);

        Stadium::insert([
            'id' => 32,
            'name' => 'Selhurst Park'
        ]);

        Stadium::insert([
            'id' => 33,
            'name' => 'Carrow Road'
        ]);

        Stadium::insert([
            'id' => 34,
            'name' => 'Darlington Football Club'
        ]);

        Stadium::insert([
            'id' => 35,
            'name' => 'МКМ Stadium'
        ]);

        Stadium::insert([
            'id' => 36,
            'name' => 'DW Stadium'
        ]);

        Stadium::insert([
            'id' => 37,
            'name' => 'Valley Parade'
        ]);

        Stadium::insert([
            'id' => 38,
            'name' => 'Rotherham United Football Club'
        ]);

        Stadium::insert([
            'id' => 39,
            'name' => 'John Smith`s Stadium'
        ]);

        Stadium::insert([
            'id' => 40,
            'name' => 'Madejski Stadium'
        ]);

        Stadium::insert([
            'id' => 41,
            'name' => 'Oakwell'
        ]);

        Stadium::insert([
            'id' => 43,
            'name' => 'Turf Moor'
        ]);

        Stadium::insert([
            'id' => 44,
            'name' => 'Port Vale Football Club'
        ]);

        Stadium::insert([
            'id' => 45,
            'name' => 'Deepdale Stadium'
        ]);

        Stadium::insert([
            'id' => 46,
            'name' => 'Stadium mk'
        ]);

        Stadium::insert([
            'id' => 47,
            'name' => 'Ashton Gate'
        ]);

        Stadium::insert([
            'id' => 48,
            'name' => 'Plymouth Argyle Football Club'
        ]);

        Stadium::insert([
            'id' => 49,
            'name' => 'Fratton Park'
        ]);

        Stadium::insert([
            'id' => 50,
            'name' => 'Stadiwm Liberty'
        ]);

        Stadium::insert([
            'id' => 51,
            'name' => 'Meadow Lane'
        ]);

        Stadium::insert([
            'id' => 52,
            'name' => 'The New Den'
        ]);

        Stadium::insert([
            'id' => 53,
            'name' => 'Vicarage Road'
        ]);

        Stadium::insert([
            'id' => 54,
            'name' => 'Loftus Road'
        ]);
    }
}
