<?php

namespace Database\Seeders;

use App\Models\Game;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Game::insert([
            'season_id' => 1,
            'team_a_id' => 1,
            'team_b_id' => 2,
            'scope_a' => 0,
            'scope_b' => 0,
            'wined_team_id' => 1,
            'stadium_id' => 1,
            'match_start_at' => Carbon::now()->toDateTime(),
            'created_at' => Carbon::now()->toDateTime(),
            'updated_at' => Carbon::now()->toDateTime()
        ]);

        Game::insert([
            'season_id' => 1,
            'team_a_id' => 2,
            'team_b_id' => 1,
            'scope_a' => 0,
            'scope_b' => 0,
            'wined_team_id' => 1,
            'stadium_id' => 1,
            'match_start_at' => Carbon::now()->addDay()->toDateTime(),
            'created_at' => Carbon::now()->toDateTime(),
            'updated_at' => Carbon::now()->toDateTime()
        ]);

        DB::table('season_team')->insert([
            'team_id' => 1,
            'season_id' => 1,
            'total_games' => 2,
            'wins' => 0,
            'losses' => 0,
            'drawn' => 2,
            'scope' => 2
        ]);

        DB::table('season_team')->insert([
            'team_id' => 2,
            'season_id' => 1,
            'total_games' => 2,
            'wins' => 0,
            'losses' => 0,
            'drawn' => 2,
            'scope' => 2
        ]);
    }
}
