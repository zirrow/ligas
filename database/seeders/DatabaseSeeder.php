<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SeasonsSeeder::class);
        $this->call(StadiumsSeeder::class);
        $this->call(TeamsSeeder::class);
        $this->call(TestSeeder::class);
    }
}
