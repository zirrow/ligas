<?php

namespace App\Services\Interfaces;

use App\Models\Season;
use App\Models\Game;

interface GameServiceInterface
{
    /**
     * @param Season $season
     * @return void
     */
    public function startSeason(Season $season): void;

    /**
     * @param Game $game
     * @return void
     */
    public function playGames(Game $game): void;

    /**
     * @return array
     */
    public function getGamesBySeasonAndWeek(Season $season, int $week): array;
}
