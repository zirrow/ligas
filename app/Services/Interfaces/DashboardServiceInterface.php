<?php

namespace App\Services\Interfaces;

use App\Models\Season;

interface DashboardServiceInterface
{
    public function getPredictionsBySeasonAndWeek(Season $season, int $week, GameServiceInterface $gameService): array;
}
