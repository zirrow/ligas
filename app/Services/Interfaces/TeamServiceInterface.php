<?php

namespace App\Services\Interfaces;

use App\Models\{Season, Team};

interface TeamServiceInterface
{
    /**
     * @param Team $team
     * @param Season $season
     * @return bool
     */
    public function addToSeason(Team $team, Season $season): bool;

    /**
     * @param Team $team
     * @param Season $season
     * @return bool
     */
    public function removeFromSeason(Team $team, Season $season): bool;
}
