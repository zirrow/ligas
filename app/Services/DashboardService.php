<?php

namespace App\Services;

use App\Models\Game;
use App\Models\Season;
use App\Models\Team;
use App\Services\Interfaces\DashboardServiceInterface;
use App\Services\Interfaces\GameServiceInterface;

class DashboardService implements DashboardServiceInterface
{
    /**
     * @param Season $season
     * @param int $week
     * @return array
     */
    public function getPredictionsBySeasonAndWeek(Season $season, int $week, GameServiceInterface $gameService): array
    {
        $games = $gameService->getGamesBySeasonAndWeek($season, $week);

        $teamsIds = [];
        foreach ($games as $game) {
            $teamsIds[] = $game['team_a_id'];
            $teamsIds[] = $game['team_b_id'];
        }

        $teamsIds = array_unique($teamsIds);

        $teams = Team::whereIn('id', $teamsIds)->get();

        $weight = $this->getWeight($teams);

        $predictions = [];

        foreach ($teams as $team) {
            $predictions[$team->id] = [
                'name' => $team->name,
                'chance' => $team->skill_level * $weight
            ];
        }

        return $predictions;
    }

    /**
     * @param array $teamsIds
     * @return int
     */
    private function getWeight($teams): int
    {
        $countOfSkillLevels = 0;
        foreach ($teams as $team) {
            $countOfSkillLevels = $countOfSkillLevels + $team->skill_level;
        }

        if ($countOfSkillLevels <= 0 ) {
            return 0;
        }

        return 100 / $countOfSkillLevels;
    }
}
