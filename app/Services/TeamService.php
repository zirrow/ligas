<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\{Season, Team};
use App\Services\Interfaces\TeamServiceInterface;

class TeamService implements TeamServiceInterface
{
    /**
     * @param Team $team
     * @param Season $season
     * @return void
     */
    public function addToSeason(Team $team, Season $season): bool
    {
        DB::beginTransaction();

        try
        {
            $team->seasons()->attach($season->id);

            DB::commit();
        }
        catch (\Exception $e)
        {
            DB::rollBack();

            Log::error('can`t add team in season', [
                'Team' => $team,
                'Season' => $season,
                'Exception' => $e
            ]);

            return false;
        }

        return true;
    }

    /**
     * @param Team $team
     * @param Season $season
     * @return void
     */
    public function removeFromSeason(Team $team, Season $season): bool
    {
        DB::beginTransaction();

        try
        {
            $team->seasons()->detach($season->id);
            DB::commit();
        }
        catch (\Exception $e)
        {
            DB::rollBack();

            Log::error('can`t delete team from season', [
                'Team' => $team,
                'Season' => $season,
                'Exception' => $e
            ]);

            return false;
        }

        return true;
    }
}
