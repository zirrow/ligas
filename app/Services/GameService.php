<?php

namespace App\Services;

use App\Models\{Game, Season, Team};
use App\Services\Interfaces\GameServiceInterface;
use Carbon\Carbon;

class GameService implements GameServiceInterface
{

    private const WEEKS_LIMIT = 36;

    /**
     * $pointMatrix[x][y] = points:
     * x - 0 - lost
     * x - 1 - won
     * x - 3 - drawn
     *
     * y - 0 - on own field
     * y - 1 - on a foreign field
     *
     * @var array[]
     */
    private const POINTS_MATRIX = [
        0 => [
            0 => 0,
            1 => 0
        ],
        1 => [
            0 => 2,
            1 => 3
        ],
        3 => [
            0 => 1,
            1 => 1
        ]
    ];

    /**
     * @param Season $season
     * @return void
     */
    public function startSeason(Season $season): void
    {
        $teams = $season->teams;

        $gamesPearWeek = $this->gamesPearWeek($teams->count());
        $weekNumber = 1;
        $gamesOnCurrentWeek = 0;

        foreach ($teams as $teamA) {
            foreach ($teams as $teamB) {

                if ($teamA->id == $teamB->id ) {
                    continue;
                }

                Game::firstOrCreate([
                    'season_id' => $season->id,
                    'team_a_id' => $teamA->id,
                    'team_b_id' => $teamB->id,
                ], [
                    'stadium_id' => $teamA->stadium_id,
                    'match_start_at' => Carbon::parse($season->start_date)->addDays($gamesOnCurrentWeek)->addWeeks($weekNumber)->toDateTime(),
                    'week_number' => $weekNumber
                ]);

                if ($gamesOnCurrentWeek < $gamesPearWeek) {
                    $gamesOnCurrentWeek++;
                } else {
                    $gamesOnCurrentWeek = 0;
                    $weekNumber++;
                }
            }
        }
    }

    /**
     * @param int $teamsCount
     * @return int
     */
    private function gamesPearWeek(int $teamsCount): int
    {
        $gamesPeerWeek = pow($teamsCount, 2) / self::WEEKS_LIMIT;

        if ($gamesPeerWeek <= 2) {
            return 2;
        }

        return $gamesPeerWeek;
    }

    /**
     * @param Season $season
     * @return void
     */
    public function playGames(Game $game): void
    {
        $gameWithTeams = $game->load(['teamA', 'teamB']);

        $teamAGoals = $this->getGoals($gameWithTeams->teamA);
        $teamBGoals = $this->getGoals($gameWithTeams->teamB);

        $wonTeamId = $this->getWonCommandId($game, $teamAGoals, $teamBGoals);

        $pointsTeamA = $this->getTeamPointsForGame($game, $gameWithTeams->teamA, $wonTeamId);
        $pointsTeamB = $this->getTeamPointsForGame($game, $gameWithTeams->teamB, $wonTeamId);

        $this->saveGameResult($game, $teamAGoals, $teamBGoals, $wonTeamId);

        $this->saveTeamPoints($gameWithTeams->teamA, $pointsTeamA, $game, $teamAGoals, $teamBGoals);
        $this->saveTeamPoints($gameWithTeams->teamB, $pointsTeamB, $game, $teamBGoals, $teamAGoals);
    }

    /**
     * @param Team $team
     * @return int
     */
    private function getGoals(Team $team): int
    {
        //Chelsea must always win
        if ($team->id == 8) {
            return 10;
        }

        return rand(0, $team->skill_level);
    }

    /**
     * @param Game $game
     * @param int $teamAGoals
     * @param int $teamBGoals
     * @return int|null
     */
    private function getWonCommandId(Game $game, int $teamAGoals, int $teamBGoals): ?int
    {
        if ($teamAGoals > $teamBGoals) {
            return $game->teamA->id;
        } elseif ($teamBGoals == $teamAGoals) {
            return null;
        }

        return $game->teamB->id;
    }

    /**
     * @param Game $game
     * @param Team $team
     * @param int|null $wonTeamId
     * @return int
     */
    private function getTeamPointsForGame(Game $game, Team $team, ?int $wonTeamId): int
    {
        if (is_null($wonTeamId)) {
            return self::POINTS_MATRIX[3][0];
        }

        $win = $team->id == $wonTeamId ? 1 : 0;
        $stadium = $game->stadium_id != $team->stadium_id ? 1 : 0;

        return self::POINTS_MATRIX[$win][$stadium];
    }

    /**
     * @param Game $game
     * @param int $teamAGoals
     * @param int $teamBGoals
     * @param int|null $wonTeamId
     * @return void
     */
    private function saveGameResult(Game $game, int $teamAGoals, int $teamBGoals, ?int $wonTeamId): void
    {
        $game->scope_a = $teamAGoals;
        $game->scope_b = $teamBGoals;
        $game->wined_team_id = $wonTeamId;
        $game->save();
    }

    /**
     * @param Team $team
     * @param int $points
     * @param Game $game
     * @return void
     */
    private function saveTeamPoints(Team $team, int $points, Game $game, int $teamAGoals, int $teamBGoals):void
    {
        $teamStats = $team->seasons()->where('season_id', $game->season_id)->first();

        $team->seasons()->updateExistingPivot($game->season_id, [
            'total_games' => $teamStats->pivot->total_games + 1,
            'wins' => $points > 1 ? $teamStats->pivot->wins + 1 : $teamStats->pivot->wins,
            'losses' => $points == 0 ? $teamStats->pivot->losses + 1 : $teamStats->pivot->losses,
            'drawn' => $points == 1 ? $teamStats->pivot->drawn + 1 : $teamStats->pivot->drawn,
            'scope' => $teamStats->pivot->scope + $points,
            'goals_count' => $teamStats->pivot->goals_count + $teamAGoals,
            'conceded_goals_count' => $teamStats->pivot->conceded_goals_count + $teamBGoals,
        ]);
    }

    /**
     * @param Season $season
     * @param int $week
     * @return array
     */
    public function getGamesBySeasonAndWeek(Season $season, int $week): array
    {
        $teams = $season->teams->pluck('id')->toArray();

        return Game::whereIn('team_a_id', $teams)
            ->whereIn('team_b_id', $teams)
            ->where('season_id', $season->id)
            ->where('week_number', $week)
            ->get()
            ->toArray();
    }
}
