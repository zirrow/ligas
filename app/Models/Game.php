<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'season_id',
        'team_a_id',
        'team_b_id',
        'scope_a',
        'scope_b',
        'wined_team_id',
        'match_start_at',
        'week_number',
        'stadium_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function teamA()
    {
        return $this->hasOne(Team::class, 'id', 'team_a_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function teamB()
    {
        return $this->hasOne(Team::class, 'id', 'team_b_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function winner()
    {
        return $this->hasOne(Team::class, 'id', 'wined_team');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function season()
    {
        return $this->hasOne(Season::class, 'id', 'season_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function stadium()
    {
        return $this->hasOne(Stadium::class, 'stadium_id', 'id');
    }
}
