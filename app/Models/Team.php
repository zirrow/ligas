<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'stadium_id',
        'skill_level'
    ];

    /**
     * @return mixed
     */
    public function getGamesAttribute()
    {
        return Game::where('team_a_id', $this->id)->orWhere('team_b_id', $this->id)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function seasons()
    {
        return $this->belongsToMany(Season::class)->withPivot([
            'id',
            'team_id',
            'season_id',
            'total_games',
            'wins',
            'losses',
            'drawn',
            'scope',
            'goals_count',
            'conceded_goals_count'
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function stadium()
    {
        return $this->hasOne(Stadium::class, 'id', 'stadium_id');
    }
}
