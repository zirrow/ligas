<?php

namespace App\Jobs;

use App\Models\Game;
use App\Services\Interfaces\GameServiceInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PlayGamesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $seasonId;

    private $gameService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $seasonId, GameServiceInterface $gameService)
    {
        $this->seasonId = $seasonId;

        $this->gameService = $gameService;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $games = Game::where('season_id', $this->seasonId)->get();

        foreach ($games as $game) {
            sleep(1);
            $this->gameService->playGames($game);
        }
    }
}
