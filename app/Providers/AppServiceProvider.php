<?php

namespace App\Providers;

use App\Http\Controllers\Composer\DefaultComposer;
use App\Services\Interfaces\{
    DashboardServiceInterface,
    GameServiceInterface,
    TeamServiceInterface
};
use App\Services\{
    DashboardService,
    TeamService,
    GameService
};
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TeamServiceInterface::class, TeamService::class);
        $this->app->bind(GameServiceInterface::class, GameService::class);
        $this->app->bind(DashboardServiceInterface::class, DashboardService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
	    View::composer('*', DefaultComposer::class);
    }
}
