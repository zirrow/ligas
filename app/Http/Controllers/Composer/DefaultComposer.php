<?php

namespace App\Http\Controllers\Composer;

use App\Http\Controllers\Controller;
use App\Models\Season;
use App\Models\Stadium;
use Illuminate\Support\Facades\App;
use Illuminate\View\View;

/**
 * Class DefaultComposer
 * @package App\Http\Controllers\Composer
 *
 * @property array $data
 */
class DefaultComposer extends Controller
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     *
     * @return void
     */
    public function compose(View $view): void
    {
        $app_data = [];

        $app_data['debug']  = config('app.env') !== 'production';
        $app_data['locale'] = App::getLocale();

        // Seasons
        $app_data['seasons'] = Season::all()->map->only(['id', 'name']);

        // Stadiums
        $app_data['stadiums'] = Stadium::all()->map->only(['id', 'name']);

        $view->with('app_data', collect($app_data));
    }
}
