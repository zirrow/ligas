<?php

namespace App\Http\Controllers;

use App\Http\Requests\Stadium\{StoreRequest, UpdateRequest};
use App\Models\Stadium;

class StadiumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return ok(Stadium::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        return forbidden();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        return ok(Stadium::create(['name' => $request->name]));
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return forbidden();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        return forbidden();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param Stadium $stadium
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, Stadium $stadium)
    {
        $stadium->name = $request->name;
        $stadium->save();

        return ok($stadium);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Stadium $stadium
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Stadium $stadium)
    {
        $stadium->delete();
        return ok();
    }
}
