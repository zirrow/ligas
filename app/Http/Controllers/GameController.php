<?php

namespace App\Http\Controllers;

use App\Jobs\PlayGamesJob;
use App\Models\Season;
use App\Services\Interfaces\GameServiceInterface;

class GameController extends Controller
{
    /**
     * @param Season $season
     * @param GameServiceInterface $gameService
     * @return \Illuminate\Http\JsonResponse
     */
    public function startSeason(Season $season, GameServiceInterface $gameService)
    {
        $gameService->startSeason($season);

        return ok(['status' => 'success']);
    }

    /**
     * @param Season $season
     * @return \Illuminate\Http\JsonResponse
     */
    public function playGames(Season $season, GameServiceInterface $gameService)
    {
        $this->dispatch(new PlayGamesJob($season->id, $gameService));

        return ok(['status' => 'success']);
    }

    /**
     * @param Season $season
     * @param int $week
     * @param GameServiceInterface $gameService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGamesBySeasonAndWeek(Season $season, int $week, GameServiceInterface $gameService)
    {
        return ok($gameService->getGamesBySeasonAndWeek($season, $week));
    }
}
