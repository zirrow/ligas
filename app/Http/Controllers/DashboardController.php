<?php

namespace App\Http\Controllers;

use App\Http\Requests\Dashboard\IndexRequest;
use App\Models\Season;
use App\Services\Interfaces\DashboardServiceInterface;
use App\Services\Interfaces\GameServiceInterface;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param IndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(IndexRequest $request)
    {
        return ok(Season::where('name', $request->season_name)->first()->teams);
    }

    /**
     * @param Season $season
     * @param int $week
     * @param DashboardServiceInterface $dashboardService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPredictionsBySeasonAndWeek(
        Season $season,
        int $week,
        DashboardServiceInterface $dashboardService,
        GameServiceInterface $gameService
    ) {
        return ok($dashboardService->getPredictionsBySeasonAndWeek($season, $week, $gameService));
    }
}
