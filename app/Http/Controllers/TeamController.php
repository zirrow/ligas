<?php

namespace App\Http\Controllers;

use App\Models\Season;
use App\Services\Interfaces\TeamServiceInterface;
use App\Http\Requests\Team\{StoreRequest, UpdateRequest};
use App\Models\Team;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return ok(Team::get()->load(['stadium', 'seasons']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        return forbidden();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        return ok(Team::create([
            'name' => $request->name,
            'stadium_id' => $request->stadium_id,
            'skill_level' => $request->skill_level
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param Team $team
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Team $team)
    {
        return ok($team->load(['seasons']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        return forbidden();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param Team $team
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, Team $team)
    {
        $team->name = $request->name;
        $team->stadium_id = $request->stadium_id;
        $team->skill_level = $request->skill_level;
        $team->save();

        return ok($team);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Team $team
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Team $team)
    {
        $team->delete();

        return ok();
    }

    /**
     * @param Team $team
     * @param Season $season
     * @param TeamServiceInterface $teamService
     * @return \Illuminate\Http\JsonResponse
     */
    public function addToSeason(Team $team, Season $season, TeamServiceInterface $teamService)
    {
        if (!$teamService->addToSeason($team, $season)) {
            return bad_request('something wrong');
        }

        return ok(['status' => 'success']);
    }

    /**
     * @param Team $team
     * @param Season $season
     * @param TeamServiceInterface $teamService
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeFromSeason(Team $team, Season $season, TeamServiceInterface $teamService)
    {
        if (!$teamService->removeFromSeason($team, $season)) {
            return bad_request('something wrong');
        }

        return ok(['status' => 'success']);
    }
}
