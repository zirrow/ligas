<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{DashboardController, TeamController, GameController};


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'bindings'], function () {
    Route::get('dashboard', [DashboardController::class, 'index']);
    Route::get('dashboard/get-predictions/{season}/{week}', [DashboardController::class, 'getPredictionsBySeasonAndWeek']);

    Route::resource('stadium', '\App\Http\Controllers\StadiumController');

    Route::resource('team', '\App\Http\Controllers\TeamController');
    Route::post('team/add-to-season/{team}/{season}', [TeamController::class, 'addToSeason']);
    Route::post('team/remove-from-season/{team}/{season}', [TeamController::class, 'removeFromSeason']);

    Route::post('game/start/{season}', [GameController::class, 'startSeason']);
    Route::post('game/play-games/{season}', [GameController::class, 'playGames']);
    Route::get('game/get-games-by-season-and-week/{season}/{week}', [GameController::class, 'getGamesBySeasonAndWeek']);
});



