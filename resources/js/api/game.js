import axios from './client';

let ENDPOINT_URL = 'game';

export default {
    weekResult: (season, week) => axios.get(`${ENDPOINT_URL}/get-games-by-season-and-week/${season}/${week}`),
    startSeason: (season) => axios.post(`${ENDPOINT_URL}/start/${season}`),
    playGames: (season) => axios.post(`${ENDPOINT_URL}/play-games/${season}`),
};
