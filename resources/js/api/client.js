import axios from 'axios';
import { API_PROTOCOL, API_HOST, API_PORT, API_BASE_PATH } from './config';

window.axios = axios;

export default axios.create({
    baseURL: `${API_PROTOCOL}//${API_HOST}:${API_PORT}${API_BASE_PATH}`,
    headers: {
        'X-Requested-With': 'XMLHttpRequest'
    },
});
