import axios from './client';

let ENDPOINT_URL = 'team';

export default {
    list: () => axios.get(`${ENDPOINT_URL}`),
    store: (data) => axios.post(`${ENDPOINT_URL}`, data),
    update: (id, data) => axios.put(`${ENDPOINT_URL}/${id}`, data),
    delete: (id) => axios.delete(`${ENDPOINT_URL}/${id}`),
    addToSeason: (team, season) => axios.post(`${ENDPOINT_URL}/add-to-season/${team}/${season}`),
    removeFromSeason: (team, season) => axios.post(`${ENDPOINT_URL}/remove-from-season/${team}/${season}`),
};
