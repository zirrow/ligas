import axios from './client';

let ENDPOINT_URL = 'dashboard';

export default {
    list: (data = null) => axios.get(`${ENDPOINT_URL}`, {params: data}),
    prediction: (season, week) => axios.get(`${ENDPOINT_URL}/get-predictions/${season}/${week}`),
};
