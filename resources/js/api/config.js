let url = window.location;

export const API_PROTOCOL = url.protocol;
export const API_HOST = url.hostname;
export const API_PORT = url.port;

export const API_BASE_PATH = '/api/';
